FROM centos

ADD nginx.repo /etc/yum.repos.d/nginx.repo

RUN yum upgrade -y
RUN yum install -y nginx

RUN echo "daemon off;" >> /etc/nginx/nginx.conf

EXPOSE 80

CMD ["/usr/sbin/nginx"]
